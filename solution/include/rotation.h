#ifndef _ROTATION_H_
#define _ROTATION_H_

#include "image.h"
#include <stdio.h>


enum status_rotation {
    ABLE_START,
    ABLE_END
};

struct image rotate( struct image const original);

#endif
